import "react-native-gesture-handler";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import React, { useEffect } from "react";
import { Alert, SafeAreaView, StatusBar } from "react-native";
import { StyleSheet } from "react-native";
import TabBar from "./app/components/TabBar";
import { NavigationContainer } from "@react-navigation/native";
import { useStore } from "./app/stores/store";
import Onboarding from "./app/components/onboarding/Onboarding";
import Navigation from "./app/components/navigation/Navigation";

const client = new QueryClient();

export default function App() {
  return (
    <QueryClientProvider client={client}>
      <SafeAreaView style={styles.container}>
        <NavigationContainer>
          <Navigation />
        </NavigationContainer>
      </SafeAreaView>
    </QueryClientProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1a1a1a",
  },
});
