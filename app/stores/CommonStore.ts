import { LemmyHttp, SortType } from "lemmy-js-client";
import { makeAutoObservable } from "mobx";

export default class CommonStore {
  token: string;
  loggedIn: boolean = false;
  baseUrl: string;
  client: LemmyHttp;
  sortType: SortType;

  constructor() {
    makeAutoObservable(this);
  }

  setClient = (client: LemmyHttp) => {
    this.client = client;
  };
  setToken = (token: string) => {
    this.token = token;
  };
  setLoggedIn = (state: boolean) => {
    this.loggedIn = state;
  };
  setBaseUrl = (url: string) => {
    this.baseUrl = url;
  };
  setSortType = (type) => {
    this.sortType = type;
  };
}
