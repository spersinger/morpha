import React, { useEffect, useState } from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import {
  Button,
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  View,
} from "react-native";
import { useStore } from "../stores/store";
import { CreateComment, CreatePost, ListCommunities } from "lemmy-js-client";
import { useQuery } from "@tanstack/react-query";
import { Picker } from "@react-native-picker/picker";
import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import * as Haptics from "expo-haptics";

function CreateReplyPage({ route }) {
  const [community, setCommunity] = useState<number>();
  const [body, setBody] = useState<string>("");
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation<StackNavigationProp<any>>();

  const { commonStore } = useStore();
  const { client, token } = commonStore;

  const doPostReply = async () => {
    setLoading(true);
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Medium);

    const Form: CreateComment = {
      auth: token,
      content: body,
      post_id: route.params.postId,
      parent_id: route.params.parentId ? route.params.parentId : null,
    };
    const postResult = await client.createComment(Form);
    if (postResult.comment_view) {
      setLoading(false);
      navigation.pop();
      navigation.push("Post", { id: postResult.comment_view.post.id });
    }
  };

  if (loading) {
    return (
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#303030",
          flex: 1,
        }}
      >
        <Text style={styles.item}>Posting...</Text>
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flexDirection: "row" }}>
        <Text style={styles.item}>Reply to</Text>
        <View
          style={{
            alignSelf: "center",
            marginLeft: "auto",
            marginRight: "10%",
          }}
        >
          <Button color={"#3498DB"} title="Reply" onPress={doPostReply} />
        </View>
      </View>
      <Text style={styles.replyText}>{route.params.parentBody}</Text>
      <TextInput
        placeholder="Content"
        placeholderTextColor={"#bcc0c3"}
        value={body}
        onChangeText={setBody}
        style={styles.inputField}
        multiline={true}
        autoFocus={true}
      />
    </SafeAreaView>
  );
}

export default CreateReplyPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#202020",
  },
  inputField: {
    height: "100%",
    backgroundColor: "#202020",
    padding: 10,
    width: "100%",
    color: "#dee3e6",
    alignSelf: "center",
  },
  item: {
    paddingHorizontal: 40,
    paddingVertical: 10,
    color: "#dee2e6",
    fontSize: 32,
    fontWeight: "bold",
  },
  replyText: {
    color: "#dee2e6",
    backgroundColor: "#222222",
  },
});
