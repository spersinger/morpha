import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import React from "react";
import Home from "./Home";
import Account from "./Account";
import { Ionicons } from "@expo/vector-icons";
import Inbox from "./Inbox";

const Tab = createBottomTabNavigator();

const TabBar = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: "#1a1a1a",
          shadowColor: "#404040",
        },
        headerTitleStyle: { color: "#dee2e6" },
        tabBarStyle: {
          backgroundColor: "#1a1a1a",
          borderTopWidth: 1,
          borderTopColor: "#404040",
        },
        tabBarActiveTintColor: "#3498db",
      }}
    >
      <Tab.Screen
        name="Feeds"
        component={Home}
        options={{
          headerShown: false,
          tabBarIcon: ({ size, focused, color }) => {
            return <Ionicons size={size} color={color} name="library" />;
          },
        }}
      />
      <Tab.Screen
        name="Inbox"
        component={Inbox}
        options={{
          tabBarIcon: ({ size, focused, color }) => {
            return <Ionicons size={size} color={color} name="mail" />;
          },
        }}
      />
      <Tab.Screen
        name="Account"
        component={Account}
        options={{
          tabBarIcon: ({ size, focused, color }) => {
            return <Ionicons size={size} color={color} name="person" />;
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default TabBar;
