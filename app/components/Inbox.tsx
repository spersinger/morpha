import React, { useEffect, useState } from "react";
import { useStore } from "../stores/store";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Linking,
  Alert,
  RefreshControl,
} from "react-native";
import {
  CommentReplyView,
  CreateComment,
  GetReplies,
  GetRepliesResponse,
} from "lemmy-js-client";
import { FlatList } from "react-native-gesture-handler";
import { Ionicons } from "@expo/vector-icons";
import { useQuery } from "@tanstack/react-query";

function Inbox() {
  const { commonStore } = useStore();
  const { client, token } = commonStore;

  const loadReplies = async () => {
    const Form: GetReplies = {
      auth: token,
      limit: 50,
    };
    return await client.getReplies(Form);
  };

  const replyQuery = useQuery<GetRepliesResponse>(["replies"], loadReplies);

  if (replyQuery.isLoading) {
    return (
      <View style={styles.container}>
        <Text>Loading</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={replyQuery.data.replies}
        refreshControl={
          <RefreshControl
            refreshing={replyQuery.isRefetching}
            onRefresh={() => replyQuery.refetch()}
            tintColor={"#dee2e6"}
          />
        }
        renderItem={({ item }) => {
          return (
            <TouchableOpacity style={styles.replyContainer}>
              <Text style={[styles.text]}>
                <Text style={{ fontWeight: "bold" }}>{item.creator.name}</Text>{" "}
                replied to your post{" "}
                <Text style={{ fontWeight: "bold" }}>{item.post.name}</Text>
              </Text>
              <View style={{ paddingVertical: 5 }} />
              <Text style={styles.altText}>{item.comment.content}</Text>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
}

export default Inbox;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#303030",
  },
  replyContainer: {
    flex: 1,
    backgroundColor: "#222222",
    color: "#EEE",
    padding: 20,
    paddingBottom: 10,
    borderBottomColor: "#303030",
    borderBottomWidth: 5,
  },
  sideContainer: {
    paddingVertical: 100,
  },
  text: {
    color: "#dee2e6",
  },
  altText: {
    color: "#888888",
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
  },
  sideAlign: {
    flexDirection: "row",
    paddingVertical: 5,
  },
  link: {
    color: "#00BC8C",
  },
  loadingStyle: {
    textAlign: "center",
    padding: 20,
    color: "#dee2e6",
  },
});
