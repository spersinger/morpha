import React, { useEffect, useState } from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import {
  Button,
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  View,
} from "react-native";
import { useStore } from "../stores/store";
import { CreatePost, ListCommunities } from "lemmy-js-client";
import { useQuery } from "@tanstack/react-query";
import { Picker } from "@react-native-picker/picker";
import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import * as Haptics from "expo-haptics";

function CreatePostPage({ route }) {
  const [community, setCommunity] = useState<number>();
  const [name, setName] = useState("");
  const [body, setBody] = useState<string>();
  const [url, setUrl] = useState<string>();
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation<StackNavigationProp<any>>();

  const { commonStore } = useStore();
  const { client, token } = commonStore;

  const getCommunities = async () => {
    const Form: ListCommunities = {
      auth: token,
      type_: "Subscribed",
    };
    return await client.listCommunities(Form);
  };
  const doPost = async () => {
    setLoading(true);
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Medium);

    const Form: CreatePost = {
      auth: token,
      name: name,
      body: body ? body : null,
      url: url ? url : null,
      community_id: community,
    };
    const postResult = await client.createPost(Form);
    if (postResult.post_view) {
      setLoading(false);
      navigation.pop();
      navigation.push("Post", { id: postResult.post_view.post.id });
    }
  };

  const getCommunitiesQuery = useQuery(["communities"], getCommunities);
  if (loading) {
    return (
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#303030",
          flex: 1,
        }}
      >
        <Text style={styles.item}>Posting...</Text>
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.item}>Create Post</Text>
      <TextInput
        placeholder="Title"
        placeholderTextColor={"#bcc0c3"}
        value={name}
        onChangeText={setName}
        style={styles.input}
      />
      <TextInput
        placeholder="Link - optional"
        placeholderTextColor={"#bcc0c3"}
        style={styles.input}
        value={url}
        onChangeText={setUrl}
      />
      <TextInput
        placeholder="Body - optionall"
        placeholderTextColor={"#bcc0c3"}
        style={styles.inputField}
        value={body}
        onChangeText={setBody}
        multiline={true}
        autoCapitalize="sentences"
      />
      <Button color={"#3498DB"} title="Post" onPress={doPost} />
      {getCommunitiesQuery.data ? (
        <Picker
          selectedValue={community}
          onValueChange={(value, index) => setCommunity(value)}
          style={{
            marginHorizontal: "20%",
            backgroundColor: "#222222",
            borderRadius: 10,
          }}
        >
          {getCommunitiesQuery.data.communities.map((community) => (
            <Picker.Item
              label={community.community.name}
              value={community.community.id}
              key={community.community.actor_id}
              color="#dee2e6"
            />
          ))}
        </Picker>
      ) : (
        <></>
      )}
    </SafeAreaView>
  );
}

export default CreatePostPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#303030",
  },
  input: {
    height: 40,
    margin: 12,
    borderBottomWidth: 1,
    borderColor: "#3498DB",
    backgroundColor: "#222222",
    borderRadius: 5,
    borderBottomEndRadius: 0,
    borderBottomStartRadius: 0,
    padding: 10,
    width: "80%",
    color: "#dee3e6",
    alignSelf: "center",
  },
  inputField: {
    height: 100,
    margin: 12,
    borderBottomWidth: 1,
    borderColor: "#3498DB",
    backgroundColor: "#222222",
    borderRadius: 5,
    borderBottomEndRadius: 0,
    borderBottomStartRadius: 0,
    padding: 10,
    width: "80%",
    color: "#dee3e6",
    alignSelf: "center",
  },
  item: {
    padding: 20,
    color: "#dee2e6",
    fontSize: 32,
    fontWeight: "bold",
  },
});
