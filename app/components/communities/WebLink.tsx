import { Ionicons } from "@expo/vector-icons";
import React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Linking,
  View,
} from "react-native";
import { truncatePost } from "../../helpers/helpers";
import { Image } from "expo-image";
import { Post } from "lemmy-js-client";

function WebLink({ post }: { post: Post }) {
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => Linking.openURL(post.url)}
    >
      {post.thumbnail_url ? (
        <Image
          source={{ uri: post.thumbnail_url }}
          style={{
            width: "100%",
            aspectRatio: 1.5,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            opacity: 0.75,
          }}
          contentFit="fill"
        />
      ) : (
        <></>
      )}
      <View style={styles.innerContainer}>
        <Ionicons color={"#dee2e6"} name="link-outline" size={16} />
        <Text style={styles.link}>{truncatePost(post.url, 30)}</Text>
        <Ionicons color={"#dee2e6"} name="chevron-forward" size={16} />
      </View>
    </TouchableOpacity>
  );
}

export default WebLink;

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    backgroundColor: "#333333",
    borderRadius: 10,
  },
  innerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    textAlign: "center",
    padding: 20,
  },
  link: {
    color: "#00BC8C",
  },
});
