import React, { useState } from "react";
import {
  Button,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import { CommunityView, ListCommunities, SortType } from "lemmy-js-client";
import Search from "../Search";
import { useStore } from "../../stores/store";
import { useQuery } from "@tanstack/react-query";
import { FlatList } from "react-native-gesture-handler";
import { FlashList } from "@shopify/flash-list";
import { Image } from "expo-image";
import { Ionicons } from "@expo/vector-icons";

function Communities() {
  const { commonStore } = useStore();
  const { client, token } = commonStore;

  const navigation = useNavigation<StackNavigationProp<any>>();

  const fetchCommunities = async () => {
    const Form: ListCommunities = {
      auth: token,
      type_: "Subscribed",
      limit: 50,
    };
    return await client.listCommunities(Form);
  };

  const communityQuery = useQuery(["communities"], fetchCommunities);

  const topBar = () => {
    return (
      <View
        style={{
          borderBottomColor: "#333333",
          borderBottomWidth: 5,
        }}
      >
        <TouchableOpacity
          style={{
            flexDirection: "row",
            borderBottomColor: "#333333",
            borderBottomWidth: 1,
            paddingVertical: 15,
            padding: 10,
          }}
          onPress={() => navigation.navigate("Posts", { viewMode: "All" })}
        >
          <Ionicons name="globe-outline" size={32} color="#00BC8C" />
          <Text style={styles.text}>All</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flexDirection: "row",
            borderBottomColor: "#333333",
            borderBottomWidth: 1,
            padding: 10,
          }}
          onPress={() => navigation.navigate("Posts", { viewMode: "Local" })}
        >
          <Ionicons name="business-outline" size={32} color="#3498db" />
          <Text style={styles.text}>Local</Text>
        </TouchableOpacity>
      </View>
    );
  };
  const renderListItem = (item: CommunityView) => {
    return (
      <TouchableOpacity
        style={{
          backgroundColor: "#202020",
          flexDirection: "row",
          borderBottomColor: "#333333",
          borderBottomWidth:1,
          padding: 10,
        }}
        onPress={() =>
          navigation.navigate("Posts", { communityId: item.community.id })
        }
      >
        {item.community.icon ? (
          <Image
            source={{ uri: item.community.icon }}
            style={{ width: 40, height: 40, borderRadius: 5 }}
          />
        ) : (
          <Image
            source={require("../../../assets/icon.png")}
            style={{ width: 40, height: 40, borderRadius: 5 }}
          />
        )}
        <Text style={styles.text}>{item.community.name}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      {communityQuery.isLoading ? (
        <Text style={styles.loadingStyle}>Loading...</Text>
      ) : (
        <FlashList
          ListHeaderComponent={topBar}
          data={communityQuery.data.communities}
          renderItem={({ item }) => renderListItem(item)}
          estimatedItemSize={36}
          refreshControl={
            <RefreshControl
              refreshing={communityQuery.isRefetching}
              onRefresh={() => communityQuery.refetch}
              tintColor={"#dee2e6"}
            />
          }
        />
      )}
    </SafeAreaView>
  );
}

export default Communities;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#202020",
    justifyContent: "flex-start",
  },
  text: {
    color: "#3498db",
    padding: 10,
  },
  loadingStyle: {
    textAlign: "center",
    padding: 20,
    color: "#dee2e6",
  },
});
