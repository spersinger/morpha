import { CreateCommentLike, CreatePostLike, PostView } from "lemmy-js-client";
import React, { Dispatch, Fragment, useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Linking,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import ImageView from "react-native-image-viewing";
import { Image } from "expo-image";
import WebLink from "../WebLink";
import { isUriImage, truncatePost } from "../../../helpers/helpers";
import { Ionicons } from "@expo/vector-icons";
import * as Haptics from "expo-haptics";
import { useStore } from "../../../stores/store";

function Post({ post }: { post: PostView }) {
  const { commonStore } = useStore();
  const { client, token } = commonStore;
  const [imageVisible, setImageVisible] = useState(false);
  const [score, setScore] = useState(post.counts.score);
  const [myVote, setMyVote] = useState(post.my_vote ? post.my_vote : 0);

  const handleVote = async (vote: 1 | 0 | -1) => {
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Medium);
    if (myVote === vote) {
      vote = 0;
    }

    const likePostForm: CreatePostLike = {
      auth: token,
      post_id: post.post.id,
      score: vote,
    };

    setScore(score + vote);
    setMyVote(vote);
    const updated = await client.likePost(likePostForm);
    setMyVote(updated.post_view.my_vote);
    setScore(updated.post_view.counts.score);
  };

  useEffect(() => {
    setMyVote(post.my_vote ? post.my_vote : 0);
    setScore(post.counts.score);
  }, [post]);

  const getColor = () => {
    if (myVote === 1) {
      return "#3498DB";
    } else if (myVote === -1) {
      return "#e74c3c";
    }
    return "#888888";
  };

  const navigation = useNavigation<StackNavigationProp<any>>();

  const onImagePress = () => {
    setImageVisible(!imageVisible);
  };

  const handleReply = (id, body) => {
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Medium);
    navigation.navigate("Create Reply", {
      postId: id,
      parentBody: body,
    });
  };

  const handleOpenCommunity = () => {
    navigation.replace("Posts", { communityId: post.community.id });
  };

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        navigation.push("Post", { id: post.post.id });
      }}
    >
      <Text style={[styles.title, styles.text]}>{post.post.name}</Text>
      {post.post.url && isUriImage(post.post.url) && !post.post.nsfw ? (
        <View>
          <TouchableOpacity onPress={onImagePress}>
            <Image
              source={{ uri: post.post.url }}
              style={{
                aspectRatio: 1,
                width: "100%",
                flex: 1,
                marginTop: 10,
                backgroundColor: "#333333",
              }}
              contentFit="contain"
            />
          </TouchableOpacity>
          <ImageView
            images={[{ uri: post.post.url.toString() }]}
            imageIndex={0}
            visible={imageVisible}
            onRequestClose={onImagePress}
            FooterComponent={() => <View style={{ marginBottom: 20 }}></View>}
          />
        </View>
      ) : (
        <Text style={styles.text}>{truncatePost(post.post.body, 200)}</Text>
      )}
      {post.post.url && !isUriImage(post.post.url) ? (
        <View style={{ paddingHorizontal: 20 }}>
          <WebLink post={post.post} />
        </View>
      ) : (
        <></>
      )}
      <TouchableOpacity
        style={styles.communityName}
        onPress={handleOpenCommunity}
      >
        <Text style={styles.altText}>
          {post.community.name}
          {post.post.nsfw || post.community.nsfw
            ? " - NSFW (open post to view)"
            : ""}
        </Text>
      </TouchableOpacity>
      <View style={styles.actionBar}>
        <Ionicons name="arrow-up-outline" size={16} color={getColor()} />
        <Text style={{ color: getColor() }}>{score}</Text>
        <View style={{ paddingHorizontal: 5 }} />
        <Ionicons name="chatbox-outline" size={16} color={"#888888"} />
        <Text style={styles.altText}>{post.counts.comments}</Text>
        <View style={{ paddingHorizontal: 5 }} />
        <Ionicons name="person-outline" size={16} color={"#888888"} />
        <Text style={styles.altText}>{post.creator.name}</Text>
        <View style={{ paddingHorizontal: 5 }} />
        <TouchableOpacity onPress={() => Linking.openURL(post.post.ap_id)}>
          <Ionicons name="exit-outline" size={16} color={"#888888"} />
        </TouchableOpacity>
        <View style={{ marginLeft: "auto", flexDirection: "row" }}>
          <TouchableOpacity onPress={() => handleVote(1)}>
            <Ionicons
              name="arrow-up"
              size={26}
              color={myVote === 1 ? "#3498DB" : "#888888"}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleVote(-1)}>
            <Ionicons
              name="arrow-down"
              size={26}
              color={myVote === -1 ? "#E74C3C" : "#888888"}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => handleReply(post.post.id, post.post.name)}
          >
            <Ionicons name="arrow-undo" size={26} color={"#888888"} />
          </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default Post;

const styles = StyleSheet.create({
  communityName: {
    marginTop: 10,
    marginBottom: 5,
    paddingHorizontal: 20,
  },
  actionBar: {
    flexDirection: "row",
    marginBottom: 10,
    paddingHorizontal: 20,
  },
  altText: {
    color: "#888888",
    paddingHorizontal: 2,
  },
  container: {
    flex: 1,
    backgroundColor: "#222222",
    color: "#EEE",
    paddingTop: 20,
    paddingBottom: 0,
    borderBottomColor: "#303030",
    borderBottomWidth: 5,
  },
  text: {
    color: "#dee2e6",
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
  },
  link: {
    color: "#00BC8C",
  },
});
