import { useInfiniteQuery } from "@tanstack/react-query";
import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  LogBox,
  RefreshControl,
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { GetPostsResponse, PostView } from "lemmy-js-client";
import Post from "./PostListItem";
import { useStore } from "../../../stores/store";
import { FlashList } from "@shopify/flash-list";

type infiniteQueryResponseData = {
  res: GetPostsResponse;
  nextPage: number;
};

function Posts({ route }) {
  const { commonStore } = useStore();
  const { client, token, sortType } = commonStore;
  const [posts, setPosts] = useState<PostView[]>();

  const fetchPosts = async ({ pageParam = 1 }) => {
    const res = await client.getPosts({
      auth: token,
      page: pageParam,
      type_: route.params.viewMode ? route.params.viewMode : null,
      community_id: route.params.communityId ? route.params.communityId : null,
      limit: 20,
      sort: sortType,
    });
    return {
      res: res,
      nextPage: pageParam + 1,
    };
  };

  const postQuery = useInfiniteQuery<infiniteQueryResponseData>({
    queryKey: ["posts", route],
    queryFn: fetchPosts,
    getNextPageParam: (lastPage, pages) => {
      return lastPage.nextPage;
    },
    refetchOnMount: "always",
    keepPreviousData: true,
  });

  if (postQuery.isLoading) {
    return (
      <View style={styles.container}>
        <Text style={styles.loadingStyle}>Loading...</Text>
      </View>
    );
  }
  if (postQuery.isError) {
    return (
      <View style={styles.container}>
        <Text style={styles.loadingStyle}>Error</Text>
      </View>
    );
  }

  const renderData = postQuery.data
    ? postQuery.data.pages.map((page) => page.res.posts).flat()
    : [];

  const renderPostItem = (item) => {
    return <Post post={item} />;
  };

  return (
    <View style={styles.container}>
      <FlashList
        data={renderData}
        keyExtractor={(item) => item.post.ap_id}
        onEndReached={() => postQuery.fetchNextPage()}
        estimatedItemSize={291}
        renderItem={({ item }) => renderPostItem(item)}
        onEndReachedThreshold={0.5}
        ListFooterComponent={() =>
          postQuery.hasNextPage ? (
            <Text style={styles.loadingStyle}>Loading...</Text>
          ) : null
        }
        refreshControl={
          <RefreshControl
            refreshing={postQuery.isRefetching}
            onRefresh={() => postQuery.refetch()}
            tintColor={"#dee2e6"}
          />
        }
      />
    </View>
  );
}

export default Posts;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#303030",
  },
  loadingStyle: {
    textAlign: "center",
    padding: 20,
    color: "#dee2e6",
  },
});
