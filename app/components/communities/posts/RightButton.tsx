import { Ionicons } from "@expo/vector-icons";
import { StackNavigationProp } from "@react-navigation/stack";
import React, { useState } from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useStore } from "../../../stores/store";
import { useNavigation } from "@react-navigation/native";
import { StyleSheet, View } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";

function RightButton() {
  const { commonStore } = useStore();
  const { setSortType, sortType } = commonStore;
  const [visible, setVisible] = useState(false);
  const [items, setItems] = useState([
    { label: "Hot", value: "Hot" },
    { label: "Active", value: "Active" },
    { label: "New", value: "New" },
  ]);

  const navigation = useNavigation<StackNavigationProp<any>>();

  const handleCreatePost = () => {
    navigation.push("Create Post");
  };
  const handleSetSortType = (value) => {
    setSortType(value);
  };

  return (
    <View style={{ flexDirection: "row" }}>
      <TouchableOpacity
        onPress={() => handleCreatePost()}
        style={{ marginRight: 10 }}
      >
        <Ionicons name="create-outline" size={26} color={"#dee2e6"} />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => setVisible(!visible)}
        style={{ marginRight: 10 }}
      >
        <Ionicons name="filter" size={26} color={"#dee2e6"} />
      </TouchableOpacity>
      <View style={styles.dropdown}>
        {visible ? (
          <DropDownPicker
            items={items}
            value={sortType}
            setOpen={setVisible}
            open={visible}
            setValue={() => {}}
            onSelectItem={(item) => handleSetSortType(item.value)}
          />
        ) : (
          <></>
        )}
      </View>
    </View>
  );
}

export default RightButton;

const styles = StyleSheet.create({
  dropdown: {
    position: "absolute",
    right: "5%",
  },
});
