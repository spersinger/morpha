import { Ionicons } from "@expo/vector-icons";
import {
  CommentResponse,
  CommentView,
  CreateCommentLike,
} from "lemmy-js-client";
import React, { useEffect, useState } from "react";
import { View, TouchableOpacity, StyleSheet, Text } from "react-native";
import { useStore } from "../../../stores/store";
import * as Haptics from "expo-haptics";
import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import { NestedComment } from "../../../helpers/helpers";
import { FlatList } from "react-native-gesture-handler";

type propTypes = {
  item: NestedComment;
};

const Comment = ({ item }: propTypes) => {
  const { commonStore } = useStore();
  const { client, token } = commonStore;
  const [score, setScore] = useState(item.comment.counts.score);
  const [myVote, setMyVote] = useState(item.comment.my_vote);

  const navigation = useNavigation<StackNavigationProp<any>>();

  const handleReply = (id, body) => {
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Medium);
    navigation.navigate("Create Reply", {
      postId: item.comment.comment.id,
      parentId: id,
      parentBody: body,
    });
  };
  const handleVote = async (vote: 1 | 0 | -1) => {
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Medium);
    if (myVote === vote) {
      vote = 0;
    }

    const likeCommentForm: CreateCommentLike = {
      auth: token,
      comment_id: item.comment.comment.id,
      score: vote,
    };

    setScore(score + vote);
    setMyVote(vote);
    const updated = await client.likeComment(likeCommentForm);
    setScore(updated.comment_view.counts.score);
    setMyVote(updated.comment_view.my_vote);
  };
  const getColor = () => {
    if (myVote === 1) {
      return "#3498DB";
    } else if (myVote === -1) {
      return "#e74c3c";
    }
    return "#888888";
  };

  const getColorFromDepth = () => {
    switch (item.depth) {
      case 0:
        return "#222222";
      case 1:
        return "#ac5353";
      case 2:
        return "#ac9d53";
      case 3:
        return "#71ac53";
      case 4:
        return "#53ab80";
      case 5:
        return "#538eab";
      default:
        return "#222222";
    }
  };

  return (
    <View
      style={[
        styles.commentContainer,
        {
          borderLeftColor: getColorFromDepth(),
          borderLeftWidth: 2,
          marginVertical: 20,
        },
      ]}
    >
      <View style={styles.sideAlign}>
        <Ionicons name="person-outline" style={styles.altText} size={16} />
        <Text style={styles.altText}>{item.comment.creator.name}</Text>
        <View style={{ paddingHorizontal: 2 }} />
        <Ionicons name="ios-arrow-up" color={getColor()} size={16} />
        <Text style={{ color: getColor() }}>{score}</Text>
        <View
          style={{
            marginLeft: "auto",
            flexDirection: "row",
            paddingRight: 10,
          }}
        >
          <TouchableOpacity onPress={() => handleVote(1)}>
            <Ionicons
              name="arrow-up"
              color={myVote === 1 ? "#3498DB" : "#888888"}
              size={24}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleVote(-1)}>
            <Ionicons
              name="arrow-down"
              color={myVote === -1 ? "#E74C3C" : "#888888"}
              size={24}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              handleReply(item.comment.comment.id, item.comment.comment.content)
            }
          >
            <Ionicons name="arrow-undo" style={styles.altText} size={24} />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.commentTextContainer}>
        <Text style={styles.text}>{item.comment.comment.content}</Text>
      </View>
      <FlatList
        data={item.replies}
        renderItem={({ item }) => <Comment item={item} />}
      />
    </View>
  );
};

export default Comment;

const styles = StyleSheet.create({
  text: {
    color: "#dee2e6",
  },
  commentTextContainer: {
    paddingBottom: 10,
    borderBottomColor: "#333333",
    borderBottomWidth: 2,
  },
  altText: {
    color: "#888888",
  },
  commentContainer: {
    paddingHorizontal: 10,
  },
  sideAlign: {
    flexDirection: "row",
    paddingVertical: 5,
  },
});
