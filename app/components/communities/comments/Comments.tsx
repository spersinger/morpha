import { GetComments, GetPost } from "lemmy-js-client";
import React, { useState } from "react";
import { useStore } from "../../../stores/store";
import { useQuery } from "@tanstack/react-query";
import { View, Text, StyleSheet } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import Post from "../posts/PostListItem";
import Comment from "./Comment";
import CommentPostView from "./CommentPostView";
import { NestedComment, threadComments } from "../../../helpers/helpers";

function Comments({ route }) {
  const { commonStore } = useStore();
  const { client, token } = commonStore;

  const [threaded, setThreaded] = useState<NestedComment[]>();

  const loadComments = async () => {
    const Form: GetComments = {
      auth: token,
      post_id: route.params.id,
      type_: "All",
      sort: "Top",
      max_depth: 15,
    };

    const comments = await client.getComments(Form);
    setThreaded(threadComments(comments.comments));
    return comments;
  };
  const loadPost = async () => {
    const Form: GetPost = {
      auth: token,
      id: route.params.id,
    };

    const post = await client.getPost(Form);
    return post;
  };

  const commentQuery = useQuery(["comments", route], loadComments);
  const postQuery = useQuery(["post", route.params.id], loadPost);

  if (commentQuery.isLoading || postQuery.isLoading) {
    return (
      <View style={styles.container}>
        <Text style={styles.loadingStyle}>Loading...</Text>
      </View>
    );
  }

  const renderCommentItem = (item) => {
    return <Comment item={item} />;
  };

  return (
    <FlatList
      style={styles.container}
      data={threaded}
      ListHeaderComponent={() => (
        <CommentPostView post={postQuery.data.post_view} />
      )}
      renderItem={({ item }) => renderCommentItem(item)}
    />
  );
}

export default Comments;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#222222",
    color: "#EEE",
    paddingTop: 5,
  },
  commentContainer: {
    borderBottomWidth: 5,
    paddingBottom: 20,
    borderBottomColor: "#303030",
  },
  text: {
    color: "#dee2e6",
  },
  altText: {
    color: "#888888",
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
  },
  sideAlign: {
    flexDirection: "row",
    paddingVertical: 5,
  },
  loadingStyle: {
    textAlign: "center",
    padding: 20,
    color: "#dee2e6",
  },
});
