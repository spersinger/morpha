import React, { useState } from "react";
import { TouchableOpacity, View, Text, StyleSheet } from "react-native";
import ImageView from "react-native-image-viewing";
import { Image } from "expo-image";
import WebLink from "../WebLink";
import { isUriImage } from "../../../helpers/helpers";
import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import { PostView } from "lemmy-js-client";
import { Ionicons } from "@expo/vector-icons";

function CommentPostView({ post }: { post: PostView }) {
  const [imageVisible, setImageVisible] = useState(false);

  const navigation = useNavigation<StackNavigationProp<any>>();

  const onImagePress = () => {
    setImageVisible(!imageVisible);
  };
  const handleOpenCommunity = () => {
    navigation.replace("Posts", { communityId: post.community.id });
  };
  return (
    <View style={styles.container}>
      <Text style={[styles.title, styles.text]}>{post.post.name}</Text>
      {post.post.url && isUriImage(post.post.url) ? (
        <>
          <TouchableOpacity onPress={onImagePress}>
            <Image
              source={{ uri: post.post.url }}
              style={{
                aspectRatio: 1,
                width: "100%",
                flex: 1,
                marginTop: 10,
                backgroundColor: "#333333",
              }}
            />
          </TouchableOpacity>
          <ImageView
            images={[{ uri: post.post.url.toString() }]}
            imageIndex={0}
            visible={imageVisible}
            onRequestClose={onImagePress}
            FooterComponent={() => <View style={{ marginBottom: 20 }}></View>}
            onLongPress={() => console.log("Saved")}
          />
        </>
      ) : (
        <></>
      )}
      <View style={{ paddingTop: 20 }} />
      <Text style={styles.text}>{post.post.body}</Text>
      {post.post.url && !isUriImage(post.post.url) ? (
        <WebLink post={post.post} />
      ) : (
        <></>
      )}
      <View style={[{ flexDirection: "row" }, styles.communityName]}>
        <Ionicons name="arrow-up" size={16} color="#888888" />
        <Text style={styles.altText}>{post.counts.score}</Text>
        <TouchableOpacity onPress={handleOpenCommunity}>
          <Text style={styles.altText}>
            {post.creator.name} in {post.community.name}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default CommentPostView;

const styles = StyleSheet.create({
  communityName: {
    marginVertical: 10,
  },
  actionBar: {
    flexDirection: "row",
    marginBottom: 10,
  },
  altText: {
    color: "#888888",
    paddingHorizontal: 2,
  },
  container: {
    flex: 1,
    backgroundColor: "#222222",
    color: "#EEE",
    padding: 20,
    paddingBottom: 0,
    borderBottomColor: "#303030",
    borderBottomWidth: 5,
  },
  text: {
    color: "#dee2e6",
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
  },
  link: {
    color: "#00BC8C",
  },
});
