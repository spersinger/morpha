import React, { useEffect } from "react";
import { useStore } from "../../stores/store";
import Onboarding from "../onboarding/Onboarding";
import TabBar from "../TabBar";
import { observer } from "mobx-react-lite";
import AsyncStorage from "@react-native-async-storage/async-storage";

function Navigation() {
  const { commonStore } = useStore();
  const { loggedIn } = commonStore;

  if (!loggedIn) return <Onboarding />;

  return <TabBar />;
}

export default observer(Navigation);
