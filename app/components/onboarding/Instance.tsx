import React, { useEffect, useState } from "react";
import {
  Text,
  Button,
  StatusBar,
  StyleSheet,
  View,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
} from "react-native";
import {
  TextInput,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
import { useStore } from "../../stores/store";
import { LemmyHttp } from "lemmy-js-client";
import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Image } from "expo-image";

function Instance() {
  const [instance, setInstance] = useState("https://vlemmy.net");
  const { commonStore } = useStore();
  const { setClient, setLoggedIn, setToken, baseUrl, setBaseUrl } = commonStore;
  const navigation = useNavigation<StackNavigationProp<any>>();

  const checkLoggedInAlready = async () => {
    const loggedIn = await AsyncStorage.getItem("loggedIn");
    if (loggedIn === "true") {
      getLoggedInItems();
    }
    return false;
  };

  const getLoggedInItems = async () => {
    const token = await AsyncStorage.getItem("token");
    const baseUrl = await AsyncStorage.getItem("baseUrl");

    setToken(token);
    setBaseUrl(baseUrl);
    const client = new LemmyHttp(baseUrl);
    setClient(client);
    setLoggedIn(true);
  };

  useEffect(() => {
    checkLoggedInAlready();
  });

  const saveInstanceAsync = async () => {
    await AsyncStorage.setItem("baseUrl", instance);
  };

  const handleSetInstance = () => {
    const newclient = new LemmyHttp(instance);
    saveInstanceAsync();
    setClient(newclient);
    navigation.navigate("Login");
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "position" : "height"}
      style={styles.container}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.inner}>
          <Text style={styles.title}>Morpha for Lemmy.</Text>
          <Image
            source={{
              uri: "https://raw.githubusercontent.com/LemmyNet/lemmy-ui/main/src/assets/icons/favicon.svg",
            }}
            style={{ width: 200, height: undefined, aspectRatio: 1 }}
          />
          <Text style={styles.item}>What instance are you using?</Text>
        </View>
      </TouchableWithoutFeedback>
      <TextInput
        autoCorrect={false}
        autoCapitalize="none"
        autoComplete="off"
        style={styles.input}
        value={instance}
        onChangeText={setInstance}
      />
      <View style={{ width: "30%", alignSelf: "center" }}>
        <Button
          color={"#3498DB"}
          title="Continue"
          onPress={handleSetInstance}
        />
      </View>
    </KeyboardAvoidingView>
  );
}

export default Instance;

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderBottomWidth: 1,
    borderColor: "#3498DB",
    backgroundColor: "#222222",
    borderRadius: 5,
    borderBottomEndRadius: 0,
    borderBottomStartRadius: 0,
    padding: 10,
    width: "80%",
    color: "#dee3e6",
    alignSelf: "center",
  },
  inner: {
    alignItems: "center",
  },
  container: {
    flex: 1,
    backgroundColor: "#333333",
    justifyContent: "center",
  },
  item: {
    backgroundColor: "#333333",
    padding: 20,
    marginVertical: 8,
    fontWeight: "bold",
    color: "#dee3e6",
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
    fontWeight: "bold",
    color: "#dee3e6",
  },
});
