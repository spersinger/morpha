import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import Instance from "./Instance";
import LoginScreen from "./Login";

const onboardingStack = createStackNavigator();

function Onboarding() {
  return (
    <onboardingStack.Navigator
      screenOptions={{
        headerStyle: { backgroundColor: "#222222" },
        headerTitleStyle: { color: "#dee2e6" },
        headerBackTitleStyle: { color: "#3498DB" },
        headerTintColor: "#3498db",
        cardShadowEnabled: false,
        headerShadowVisible: false,
      }}
    >
      <onboardingStack.Screen name="Instance" component={Instance} />
      <onboardingStack.Screen name="Login" component={LoginScreen} />
    </onboardingStack.Navigator>
  );
}

export default Onboarding;
