import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  Alert,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { useStore } from "../../stores/store";
import { Login } from "lemmy-js-client";
import AsyncStorage from "@react-native-async-storage/async-storage";

function LoginScreen() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { commonStore } = useStore();
  const { setToken, client, setLoggedIn, token } = commonStore;

  const saveAsyncStorage = async (jwt: string) => {
    await AsyncStorage.setItem("token", jwt);
    await AsyncStorage.setItem("loggedIn", "true");
  };

  const fetchJwt = async () => {
    const Form: Login = {
      username_or_email: username,
      password: password,
    };

    try {
      await client.login(Form).then((LoginResponse) => {
        setToken(LoginResponse.jwt);
        setLoggedIn(true);
        saveAsyncStorage(LoginResponse.jwt);
      });
    } catch (err) {
      Alert.alert(
        "Something went wrong. Check your username, password, or instance url"
      );
      console.log(err);
    }
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "position" : "height"}
      style={styles.container}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.inner}>
          <Text style={styles.title}>login</Text>
          <TextInput
            style={styles.input}
            onChangeText={setUsername}
            value={username}
            placeholder="Username or email"
            autoComplete="email"
            placeholderTextColor={"#bcc0c3"}
          />
          <TextInput
            style={styles.input}
            onChangeText={setPassword}
            value={password}
            placeholder="Password"
            secureTextEntry={true}
            autoComplete="password"
            placeholderTextColor={"#bcc0c3"}
          />
        </View>
      </TouchableWithoutFeedback>
      <View style={{ width: "30%", alignSelf: "center" }}>
        <Button color={"#3498DB"} onPress={fetchJwt} title="Login" />
      </View>
    </KeyboardAvoidingView>
  );
}

export default LoginScreen;

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderBottomWidth: 1,
    borderColor: "#3498DB",
    backgroundColor: "#222222",
    borderRadius: 5,
    borderBottomEndRadius: 0,
    borderBottomStartRadius: 0,
    padding: 10,
    width: "80%",
    color: "#dee3e6",
  },
  inner: {
    alignItems: "center",
  },
  container: {
    flex: 1,
    backgroundColor: "#333333",
    justifyContent: "center",
  },
  item: {
    backgroundColor: "#333333",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
    fontWeight: "bold",
    color: "#dee3e6",
  },
});
