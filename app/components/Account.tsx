import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import { useStore } from "../stores/store";
import { GetSite, LocalUserView } from "lemmy-js-client";
import { Button, FlatList, Text, View } from "react-native";
import { StyleSheet } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useQueries, useQuery } from "@tanstack/react-query";

function Account() {
  const { commonStore } = useStore();
  const { token, setToken, setLoggedIn, loggedIn, client } = commonStore;

  const clearAsyncStorage = async () => {
    await AsyncStorage.multiRemove(["token", "baseUrl", "loggedIn"]);
  };

  const logOut = () => {
    setToken("");
    clearAsyncStorage();
    setLoggedIn(false);
  };

  const getAccountDetails = async () => {
    const Form: GetSite = {
      auth: token,
    };

    const siteDetails = await client.getSite(Form);
    return siteDetails;
  };

  const accountQuery = useQuery(["account"], getAccountDetails);

  if (accountQuery.isLoading) {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Loading</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        Username: {accountQuery.data.my_user.local_user_view.person.name}
      </Text>
      <Text style={styles.text}>
        Posts: {accountQuery.data.my_user.local_user_view.counts.post_count}
      </Text>
      <Text style={styles.text}>
        Total score:{" "}
        {accountQuery.data.my_user.local_user_view.counts.post_score +
          accountQuery.data.my_user.local_user_view.counts.comment_score}
      </Text>
      <Text style={styles.text}>
        UserId: {accountQuery.data.my_user.local_user_view.counts.id}
      </Text>
      <Button color={"#3498db"} title="Logout" onPress={logOut} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#333333",
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "#dee2e6",
  },
});

export default observer(Account);
