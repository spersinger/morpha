import React, { useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Communities from "./communities/Communities";
import Posts from "./communities/posts/Posts";
import Comments from "./communities/comments/Comments";
import RightButton from "./communities/posts/RightButton";
import CreatePostPage from "./CreatePostPage";
import CreateReplyPage from "./CreateReplyPage";

const HomeNavigator = createStackNavigator();

const Home = () => {
  return (
    <HomeNavigator.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: "#1a1a1a",
          shadowColor: "#404040",
        },
        headerTitleStyle: { color: "#dee2e6" },
        headerBackTitleStyle: { color: "#3498DB" },
        headerTintColor: "#3498db",
        cardShadowEnabled: false,
        headerRight: () => <RightButton />,
      }}
    >
      <HomeNavigator.Screen name="Communities" component={Communities} />
      <HomeNavigator.Screen name="Posts" component={Posts} />
      <HomeNavigator.Screen name="Post" component={Comments} />
      <HomeNavigator.Screen name="Create Post" component={CreatePostPage} />
      <HomeNavigator.Screen name="Create Reply" component={CreateReplyPage} />
    </HomeNavigator.Navigator>
  );
};

export default Home;
