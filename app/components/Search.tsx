import React, { useEffect, useState } from "react";
import { useStore } from "../stores/store";
import {
  Button,
  StyleSheet,
  View,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";

function Search() {
  const [search, setSearch] = useState("");
  const navigation = useNavigation<StackNavigationProp<any>>();

  return (
    <View style={styles.container}>
      <TextInput style={styles.input} placeholder="community@instance" onChangeText={setSearch} value={search} placeholderTextColor={'#bcc0c3'}/>
      <Button onPress={() => navigation.push("Posts", {communityName: search})} title="Search"/>
    </View>
  );
}

export default Search;

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderBottomWidth: 1,
    borderColor: "#3498DB",
    backgroundColor: "#222222",
    borderRadius: 5,
    borderBottomEndRadius: 0,
    borderBottomStartRadius: 0,
    padding: 10,
    width: "80%",
    color: "#dee3e6",
  },
  container: {
    flex: 1,
    backgroundColor: "#333333",
    justifyContent: 'center',
  },
});
