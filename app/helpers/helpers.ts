import { CommentReplyView, CommentView } from "lemmy-js-client";

export const truncatePost = (post: string, length: number): string => {
  if (!post) return "";

  if (post.length <= length) return post;

  return post.slice(0, length) + "...";
};

export const isUriImage = (uri: string) => {
  uri = uri.split("?")[0];
  var parts = uri.split(".");
  var extension = parts[parts.length - 1];
  var imageTypes = ["jpg", "jpeg", "tiff", "png", "gif", "bmp"];
  if (imageTypes.indexOf(extension) !== -1) {
    return true;
  }
};

export type NestedComment = {
  comment: CommentView | CommentReplyView;
  replies: NestedComment[];
  collapsed: boolean;
  depth: number;
};

export const threadComments = (comments: CommentView[]) => {
  const nestedComments = [];
  const commentDict = {};

  for (const comment of comments) {
    const { path } = comment.comment;
    const pathIds = path.split(".").map(Number);
    const depth = pathIds.length - 2;
    const parentId = pathIds[depth];

    const currentComment = {
      comment,
      replies: [],
      depth: depth,
    };

    commentDict[comment.comment.id] = currentComment;
    if (parentId !== 0) {
      const parentComment = commentDict[parentId];

      try {
        parentComment.replies.push(currentComment);
      } catch (e) {
        console.log("Error: " + e);
      }
    } else {
      nestedComments.push(currentComment);
    }
  }
  return nestedComments;
};
